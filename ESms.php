<?php

/**
 * User: gg
 * Date: 13.11.2014
 * Time: 19:24
 */
class ESms extends CApplicationComponent
{
    /**
     * @var array
     */
    public $services = array();

    public function init()
    {
        if (!Yii::getPathOfAlias('esms')) {
            Yii::setPathOfAlias('esms', dirname(__FILE__));
        }
        Yii::import('esms.*');
        Yii::import('esms.services.*');
    }

    public function getServices()
    {
        $services = false;

        if (false === $services || !is_array($services)) {
            $services = array();
            foreach ($this->services as $service => $options) {
                $class = $this->getIdentity($service, $options);
                $services[$service] = (object)array(
                    'id' => $class->getServiceName(),
                    'title' => $class->getServiceTitle(),
                );
            }
        }
        return $services;
    }

    /**
     * Returns the settings of the service.
     *
     * @param string $service the service name.
     * @return array the service settings.
     */
    protected function getService($service)
    {
        $service = strtolower($service);
        $services = $this->getServices();
        if (!isset($services[$service])) {
            throw new ESmsException(Yii::t('esms', 'Undefined service name: {service}.', array('{service}' => $service)), 500);
        }
        return $services[$service];
    }

    /**
     * Returns the service identity class.
     *
     * @param string $service the service name.
     * @param array [optional] $options
     * @return ISmsService the identity class.
     */
    public function getIdentity($service, $options = array())
    {
        $service = strtolower($service);
        if (!isset($this->services[$service])) {
            throw new ESmsException(Yii::t('esms', 'Undefined service name: {service}.', array('{service}' => $service)), 500);
        }
        $service = $this->services[$service];
        $class = $service['class'];
        $point = strrpos($class, '.');
        // if it is yii path alias
        if ($point > 0) {
            Yii::import($class);
            $class = substr($class, $point + 1);
        }
        unset($service['class']);
        $identity = new $class();
        $identity->init($this, array_merge($service, $options));
        return $identity;
    }
}