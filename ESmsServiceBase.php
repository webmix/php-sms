<?php

/**
 * User: gg
 * Date: 13.11.2014
 * Time: 20:11
 */
abstract class ESmsServiceBase extends CComponent implements ISmsService
{
    protected $name;
    protected $title;
    private $component;

    abstract public function sendSMS($phone, $text);

    /**
     * Initialize the component.
     *
     * @param ESms $component the component instance.
     * @param array $options properties initialization.
     */
    public function init($component, $options = array())
    {
        if (isset($component)) {
            $this->setComponent($component);
        }
        foreach ($options as $key => $val) {
            $this->$key = $val;
        }
    }

    /**
     * Returns service name(id).
     *
     * @return string the service name(id).
     */
    public function getServiceName()
    {
        return $this->name;
    }

    /**
     * Returns service title.
     *
     * @return string the service title.
     */
    public function getServiceTitle()
    {
        return Yii::t('esms', $this->title);
    }

    /**
     * Sets {@link ESms} application component
     *
     * @param ESms $component the application auth component.
     */
    public function setComponent($component)
    {
        $this->component = $component;
    }

    /**
     * Returns the {@link ESms} application component.
     *
     * @return ESms the {@link ESms} application component.
     */
    public function getComponent()
    {
        return $this->component;
    }

}