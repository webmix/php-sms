<?php

/**
 * User: gg
 * Date: 13.11.2014
 * Time: 19:44
 */
interface ISmsService
{
    /**
     * Initizlize the component.
     *
     * @param EAuth $component the component instance.
     * @param array $options properties initialization.
     */
    public function init($component, $options = array());

    /**
     * Returns service name(id).
     */
    public function getServiceName();

    /**
     * Returns service title.
     */
    public function getServiceTitle();

    /**
     * Sets {@link ESms} application component
     *
     * @param ESms $component the application auth component.
     */
    public function setComponent($component);

    /**
     * Returns the {@link ESms} application component.
     */
    public function getComponent();

} 