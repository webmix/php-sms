<?php

/**
 * User: gg
 * Date: 13.11.2014
 * Time: 19:55
 */
class EpochtaSmsService extends ESmsServiceBase
{
    protected $name = 'epochta';
    protected $title = 'Epochta';

    public $privateKey;
    public $publicKey;
    public $testMode;
    public $url = 'http://atompark.com/api/sms/<v>/<c>';
    private $version = '3.0';

    protected function execCommand($command, $params)
    {
        $params['key'] = $this->publicKey;
        if ($this->testMode === true) {
            $params['test'] = true;
        }
        $controlSUM = $this->calcControlSum($params, $command);
        $params['sum'] = $controlSUM;
        $url = str_replace(array('<v>', '<c>'), array($this->version, $command), $this->url);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_URL, $url);
        $body = curl_exec($curl);
        $result = new \StdClass();
        $result->status_code = curl_errno($curl);
        $result->status_message = curl_error($curl);
        $result->body = $body;

        curl_close($curl);

        return self::processResult($result);
    }

    protected static function processResult($result)
    {
        if ($result->status_code > 0) {
            throw new ESmsException($result->status_message);
        }
        $body = json_decode($result->body);
        if ($body === null) {
            throw new ESmsException('неверный ответ');
        }
        if (property_exists($body, 'error')) {
            throw new ESmsException($body->code . ': ' . $body->error);
        }

        return $body;
    }

    private function calcControlSum($params, $action)
    {
        $params['version'] = $this->version;
        $params['action'] = $action;
        ksort($params);
        $sum = '';
        foreach ($params as $k => $v)
            $sum .= $v;
        $sum .= $this->privateKey;
        return md5($sum);
    }

    /**
     * quick send sms. No list using, just 1 phone
     * @param $phone string Номер получателя
     * @param $text string Текст сообщения
     * @param $sender string Идентификатор отправителя
     * @param $datetime string '2012-05-01 00:20:00' Для планировки рассылки на заданное время
     * @param $sms_lifetime integer Время жизни смс (0 = максимум, 1, 6, 12, 24 часа)
     * @return mixed
     */
    public function sendSMS($phone, $text, $sender = 'Info', $datetime = '', $sms_lifetime = 0)
    {
        return $this->execCommand('sendSMS', array('sender' => $sender, 'text' => $text, 'phone' => $phone, 'datetime' => $datetime, 'sms_lifetime' => $sms_lifetime));
    }

    //Получить информацию о рассылке
    public function getCampaignInfo($id)
    {
        return $this->execCommand('getCampaignInfo', array('id' => $id));
    }

    public function getUserBalance($currency = null)
    {
        $body = $this->execCommand('getUserBalance', array('currency' => $currency));
        if (isset($body->result, $body->result->balance_currency)) {
            return $body->result->balance_currency;
        } else {
            return 0;
        }
    }
} 