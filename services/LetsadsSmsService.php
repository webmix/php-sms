<?php

/**
 * User: gg
 * Date: 07.04.2015
 * Time: 16:05
 */
class LetsadsSmsService extends ESmsServiceBase
{
    protected $name = 'letsads';
    protected $title = 'Letsads';

    public $login;
    public $password;
    public $url = 'http://letsads.com/api';

    /**
     * @param string $xml
     * @return mixed
     * @throws ESmsException
     */
    protected function execCommand($xml)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_URL, $this->url);
        $body = curl_exec($curl);
        $result = new \StdClass();
        $result->status_code = curl_errno($curl);
        $result->status_message = curl_error($curl);
        $result->body = $body;

        curl_close($curl);

        return self::processResult($result);
    }

    protected static function processResult($result)
    {
        if ($result->status_code > 0) {
            throw new ESmsException($result->status_message);
        }
//        var_dump($result->body);
        if (preg_match('#<name>Error</name>.*<description>(.*?)</description>#s', $result->body, $matches)) {
            $errorsList = self::getErrorsList();
            $errorMessage = isset($errorsList[$matches[1]]) ? $errorsList[$matches[1]] : 'Неизвестная ошибка (не найдена среди представленных)';
            throw new ESmsException($errorMessage);
        }
        return $result->body;
    }

    /**
     * quick send sms. No list using, just 1 phone
     * @param $phone string|array Номер получателя
     * @param $text string Текст сообщения
     * @param $sender string Идентификатор отправителя
     * @return mixed
     */
    public function sendSMS($phone, $text, $sender = 'Info')
    {
        /* create a dom document with encoding utf8 */
        $domtree = new DOMDocument('1.0', 'UTF-8');
        $xmlRoot = $domtree->createElement('request');
        $xmlRoot = $domtree->appendChild($xmlRoot);

        $auth = $domtree->createElement('auth');
        $auth = $xmlRoot->appendChild($auth);
        $auth->appendChild($domtree->createElement('login', $this->login));
        $auth->appendChild($domtree->createElement('password', $this->password));

        $message = $domtree->createElement('message');
        $message = $xmlRoot->appendChild($message);
        $message->appendChild($domtree->createElement('from', $sender));
        $message->appendChild($domtree->createElement('text', $text));
        if (is_array($phone)) {
            foreach ($phone as $phoneValue) {
                $message->appendChild($domtree->createElement('recipient', trim($phoneValue, '+')));
            }
        } else {
            $message->appendChild($domtree->createElement('recipient', trim($phone, '+')));
        }

        $xml = $domtree->saveXML();
//        var_dump($xml);
//        exit();

        return $this->execCommand($xml);
    }

    public function getUserBalance()
    {
        /* create a dom document with encoding utf8 */
        $domtree = new DOMDocument('1.0', 'UTF-8');
        $xmlRoot = $domtree->createElement('request');
        $xmlRoot = $domtree->appendChild($xmlRoot);

        $auth = $domtree->createElement('auth');
        $auth = $xmlRoot->appendChild($auth);
        $auth->appendChild($domtree->createElement('login', $this->login));
        $auth->appendChild($domtree->createElement('password', $this->password));

        $balance = $domtree->createElement('balance');
        $xmlRoot->appendChild($balance);

        $xml = $domtree->saveXML();
//        var_dump($xml);
//        exit();

        $result = $this->execCommand($xml);
        if (preg_match('#<name>Balance</name>.*<description>(.*?)</description>.*<currency>(.*?)</currency>#s', $result, $matches)) {
            return $matches[1];
        } else {
            return 0;
        }
    }

    public static function getErrorsList()
    {
        return array(
            'NO_DATA' => 'Ошибка данных: не передан XML',
            'WRONG_DATA_FORMAT' => 'Ошибка формата переданного XML',
            'REQUEST_FORMAT' => 'Неправильный тип запроса',
            'AUTH_DATA' => 'Ошибка авторизации: несуществующий пользователь, неправильная пара логин-пароль',
            'API_DISABLED' => 'Для учетной записи пользователя запрещена работа с API',
            'USER_NOT_MODERATED' => 'Запрещена отправка сообщений без проверки для учетной записи пользователя',
            'INCORRECT_FROM' => 'Некорректное имя отправителя',
            'INVALID_FROM' => 'Несуществующее имя отправителя для данной учетной записи',
            'MESSAGE_TOO_LONG' => 'Превышена максимальная длина сообщения: 201 для кириллицы, 459 для латиницы',
            'NO_MESSAGE' => 'Пустое сообщение для оправки',
            'MAX_MESSAGES_COUNT' => 'Превышено максимальное количество респондентов в одном запросе',
            'NOT_ENOUGH_MONEY' => 'Недостаточно средств для отправки сообщения респондентам в запросе',
            'UNKNOWN_ERROR' => 'Неизвестная ошибка',
            'MESSAGE_NOT_EXIST' => 'Сообщение с заданным id не существует'
        );
    }
}